# React 笔记

## 注意点：

- react 的事件处理函数中获得的事件对象是包装后的对象，存在事件池中，不能异步访问，如果需要异步访问，需要执行 event.persist() 但是会消耗性能
  解决方法：将事件中需要获取的值保存下来，再在异步中进行使用
- 在 render 函数中需要用 this
- 子组件的 componentDidMount 比父组件先执行，因为是使用递归进行生成，所以等子组件渲染完成之后才会在父组件继续渲染。
- setState 只有在 shouldComponentUpdate 返回 false 时才不会执行重新渲染
- shouldComponentUpdate 只有两种情况下会被调用, State 或者 prop 改变时，首次渲染和 forceUpdate 都不会调用到。
- React.forwardRef 用来获取某个组件中的子组件中的 DOM 节点句柄 例：

  ```javascript
  let ref = React.createRef();
  let 组件名 = React.forwardRef((props, ref) => {
    <button ref={ref}></button>;
  });
  <组件名 ref={ref} />;
  ```

  这样就可以在外层获取到内层 button 的句柄，
  React 中 ref 写的回调的 this 直接就是组件的 this

* 组件的 render 方法返回 null 并不会影响该组件生命周期方法的回调。例如，componentWillUpdate 和 componentDidUpdate 依然可以被调用。
* class 组件只要使用了 setState 就会触发 render 函数，而函数式组件，props 改变时才会触发 render 函数的调用(因为函数式组件没有 State)。
* react 父组件传递给子组件的 props 在子组件中更新需要在 componentWillReceiveProps`(废弃)` 中更新，不能在 componentWillUpdate`(废弃)` 和 componentDidUpdate 中进行更改，会导致无限循环
* shouldComponentUpdate 方法的参数为新的 props 和 state
* 每个组件最好加上 propTypes ，判断传入参数的类型和必要的传入参数，需要导入 prop-types 包 例如：

```javascript
    组件名.porpTypes = { a : PropTypes.number , b : PropTypes.func , c : PropTypes.string.isRequire }  // isRequire代表必要的参数，不传就会报错
    props还可以设置默认值：组件名.defaultProps = { d : "defaultValue" }设置参数为数组和其中的东西

    TodoList.propTypes = {
      todos: PropTypes.arrayOf(                //参数设置为数组， arrayOf方法为数组，
        PropTypes.shape({                      //其中的对象的构造， shape方法为对象，需要传入对象
          id: PropTypes.number.isRequired,
          completed: PropTypes.bool.isRequired,
          text: PropTypes.string.isRequired
        }).isRequired
      ).isRequired,
      onTodoClick: PropTypes.func.isRequired
    }
```

**React 避免样式污染**
- 使用 Css Modules 需要在 WebPack 中添加。
```javascript
// xxx.css
.bgcolor{
  background-color:red;
}

// 某个组件中 
import style from './xxx.css'
export default ()=>{
 return <div className={style.bgcolor}/>
}
```

**Render Props**
- 也就是父组件决定子组件渲染的内容，给子组件传递一个 render props ，然后子元素直接在返回里面使用
```javascript
function Father(){
  return <Son render={(sonState)=>{
    return <div>{sonState}</div>
  }}/>
}
function Son(props){
  const myState = useState(1)
  return <div>
    {props.render(myState)}
  </div>
}
```

**setState 相关**

- 第二个参数是一个函数，第二个参数是一个回调，在设置后进行调用，所以可以输出改变后的 state：setState((state , props )=>{ ... } , ()=>{ console.log(this.state) } )
- setState 传入函数的话，第一个参数为 preState 第二个参数为传入的新的 newProps

**setState 是同步的还是异步的？什么时候同步？什么时候异步？**
- setState 中有一个 isBatchingUpdates 参数，默认为 false ，表示是否放入异步队列中进行更新，如果正常使用是为 false 所以是同步更新，但是如果放在事件中使用，React 在处理事件函数的时候会调用 batchedUpdates 函数，此函数会把 isBatchingUpdates 更新为 true ，所以就是异步的。
- 总结：由React控制的事件处理程序，以及生命周期函数调用setState是异步更新的 。React控制之外的事件中调用setState是同步更新的。比如原生js绑定的事件，setTimeout/setInterval等。

**组件绑定方法的三种方式**

1. <组件 onClick={ this.handleClick }/> 此外在组件的 constructor 中还要加上 this.handleClick = this.handleClick.bind(this);或者使用箭头函数 handleClick =()=>{console.log(123);}
2. <组件 onClick={ this.handleClick.bind(this) }/> 会影响性能,原因：如果是传递给子组件此方法，会在每次渲染时都传递一个新的（若不传递给，则没问题，但是最好不要使用）
   <组件 onClick={ ()=> this.handleClick() }/> 会影响性能,原因：如果是传递给子组件此方法，会在每次渲染时都传递一个新的（若不传递给，则没问题，但是最好不要使用）
3. （最好）使用<组件 onClick={ this.handleClick } data-xxx = {参数 1} /> 在处理方法中 handleClick(e){ e.currentTarget.dataset.xxx } 这种方法，目前找到这是性能最好的方法，（注意：其中的 xxx 需要全为小写，不能用驼峰命名法）

   **注意**

   - 最好不要在 onClick 中直接使用箭头函数 例如 onClick={()=>{...}} 这样会导致每个组件都在内存中创建一个匿名函数，浪费内存
   - 绑定事件传参最后一个参数总会是事件对象 例：<组件 onClick={ this.handleClick.bind(this,参数 1,参数 2,event 对象) }

**事件传参的三种方法**

1. <组件 onClick={ this.handleClick.bind(this,参数 1,参数 2) }/> //不好，费内存，每次渲染都传新的函数进去
2. <组件 onClick={ ()=>this.handleClick(参数 1,参数 2) }/> // 不好，费内存，每次渲染都传新的函数进去
3. （最好） <组件 onClick={ this.handleClick } data-xxx = {参数 1} /> 在处理方法中 handleClick(e){ e.currentTarget.dataset.xxx }

   **注意**

   - dom 中添加属性 data-xxx 时，不能使用驼峰命名，需要全部小写，否则会报错，不能放对象，如果需要对象的值，最好全部取出存放

**事件池**

React 事件都是合成事件，采用的是事件委托机制，都是绑定在 document 对象中的，等待事件冒泡后进行响应，而且相应的顺序也跟
普通 DOM 绑定的事件顺序一致，也是从内到外，另外，事件中 e.preventDefault() e.stopPropagation() 只对合成事件起作用，
stopPropagation 不会阻挡浏览器原生事件的冒泡。

**生命周期**

1. 初始化时：constructor -> componentWillMount`(废弃)` -> render -> componentDidMount
2. 更新 props 阶段： componentWillReceiveProps(newPrps,空对象)`(废弃)` -> shouldComponentUpdate(newProps, newState,空对象) -> componentWillUpdate（newProps, newState,空对象）`(废弃)` -> render -> componentDidUpdate(prevProps, prevState)

   如果没有 props 则没有 componentWillReceiveProps

3. 卸载阶段： componentWillUnmount

- 其中，若没有设置 state 则获取到的 newState 为 null，而空对象不知道是什么，测试的时候获取到的就是一个 {}

**React Fiber**

- 是什么？将原本的全部同步更新 DOM 改成了分步骤更新，优化了性能。
- 为什么？同步更新的缺点： 如果结构太复杂，更新的时间很长，线程一直处理更新，不能响应用户操作，给用户体验很不好。
- 详解：将 DOM 更新分为两部分，第一个阶段 Reconciliation Phase 和第二阶段 Commit Phase。第一阶段可以打断，但是第二阶段不可以。
- 受第一阶段影响的周期函数有 componentWillMount，componentWillReceiveProps，shouldComponentUpdate，componentWillUpdate。第二阶段的有 componentDidMount，componentDidUpdate，componentWillUnmount
- 注意点： componentWillMount，componentWillUpdate 这两个生命周期可能会多次执行，因为如果执行了第一阶段，然后有优先级更高的操作出现（如用户操作），会将第一阶段作废，等操作结束后再次执行第一阶段。

  **这也是为什么 AJAX 要放在 componentDidMount 中而不放在 componentWillMount 的原因.**

- 新增 getDerivedStateFromProps ，getDerivedStateFromError，componentDidCatch 生命周期。
- getDerivedStateFromProps 这个函数是一个 static 函数，也是一个纯函数，里面不能通过 this 访问到当前组件（强制避免一些有副作用的操作），输入只能通过参数，对组件渲染的影响只能通过返回值。
- 如果异常发生在 render 阶段，React 就会调用 getDerivedStateFromError，如果异常发生在第 commit 阶段，React 会调用 componentDidCatch。
- 以上的区别：除了发生的时间不同，还有 componentDidCatch 是不会在服务器端渲染的时候被调用的 而 getDerivedStateFromError 会。

**class 中 constructor 的目的**

- 为了初始化 state 和 进行方法的绑定（this.fn = this.fn.bind(this)），如果不需要以上两个操作，可以省略 constructor ，省略了还是可以直接用 fn = e=>{} （ES7 方法）来定义函数并绑定 this。

**如果需要根据 props 更新某些值，需要使用 memoization**

- 这是一个包装函数，将要处理的数据包装起来，如果结果与上一次的结果一样，什么都不做，连包装函数都不进入。

```javascript
import memoize from "memoize-one";

<Test num={Math.floor(Math.random() * 10)} />; //父组件中

class Test extends React.Component {
  getNum = memoize((num) => {
    //此处将 getNum 方法包装，如果结果跟上一次的一样，就不进行渲染，连此函数都不会进入。
    return num;
  });

  render() {
    let num = this.getNum(this.props.num); // 如果传入的值与上次一样，则直接返回上次的结果
    return <h1>{num}</h1>;
  }
}
```

**React hooks**

- hooks 是用闭包实现的，因为纯函数不能记住状态，只能通过闭包来实现。
- useSatate 中的状态是通过单向链表，fiber tree 就是一个单向链表的树形结构。
- 其中每次渲染都是一个新的函数执行，从 useState 中取出数据进行渲染，例如
- setXXX 虽然不是异步的，但是在一个函数中调用多次只会触发一次渲染，结果是最后那个
```javascript
const fn = ()=>{
  const [a,setA] = useState(1);
  const ref = useRef();
  const [count, dispatch] = useReducer(reducer, 0);
}
每次渲染得到的函数为
const fn = ()=>{
  const a = 1;
  const ref = useRef(); // useRef 每次都会取到同一个对象，可以将常用的数据，每次渲染不变的数据放在 ref.current 中
  const [count, dispatch] = useReducer(reducer, 0); // 这个每次也是取出新的值，dispatch 永远都会准确改变 count 的值
}
所以每次 state 更新都是一个新的函数执行，
```
- useEffect 的第二个参数每次都传入新的数组进去，然后对数据中的东西进行比较，如果相同则不执行回调，不相同则执行。useCallback 等同理。

**为什么只能在函数组件中使用hooks？**
- 只有函数组件的更新才会触发renderWithHooks函数，处理Hooks相关逻辑。
还是以setState为例，类组件和函数组件重新渲染的逻辑不同 ：
类组件： 用setState触发updater，重新执行组件中的render方法
函数组件： 用useState返回的setter函数来dispatch一个update action，触发更新(dispatchAction最后的scheduleWork)，用updateReducer处理更新逻辑，返回最新的state值(与Redux比较像)

**useState整体运作流程总结**
初始化： 构建dispatcher函数和初始值

更新时：
1. 调用dispatcher函数，按序插入update(其实就是一个action)
2. 收集update，调度一次React的更新
3. 在更新的过程中将ReactCurrentDispatcher.current指向负责更新的Dispatcher,执行到函数组件App()时，useState会被重新执行，在resolve dispatcher的阶段拿到了负责更新的dispatcher。
4. useState会拿到Hook对象，Hook.query中存储了更新队列，依次进行更新后，即可拿到最新的state
5. 函数组件App()执行后返回的nextChild中的count值已经是最新的了。FiberNode中的memorizedState也被设置为最新的state
6. Fiber渲染出真实DOM。更新结束。

**为什么 react 要做成异步的呢？**

1. 因为 state 更新会导致组件重新渲染，在渲染后，才能把新的 props 传递到子组件上，所以即使 state 做成同步，props 也做不成，为了保证 state 和 props 的一致性，例子：子组件调用父组件的某个函数，改变传入子组件的 props ，父组件不会同步更新 props ，子组件拿到的 props 都是不变的。
   为了性能优化，state 会根据不同的优先级进行更新。
2. 为了让 react 更加灵活，如实现异步过渡，例如页面在切换的时候，如果延时很小，就在后台自动渲染了，渲染好之后再进行跳转；如果延时相对较长，可以加一个 loading
