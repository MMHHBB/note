// const cluster = require('cluster');
// const http = require('http');
// const numCPUs = require('os').cpus().length;

// console.log(process.pid,'  id');
// if (cluster.isMaster) {
//   console.log(`主进程 ${process.pid} 正在运行`);

//   // 衍生工作进程。
//   for (let i = 0; i < numCPUs; i++) {
//     cluster.fork();
//   }

//   cluster.on('exit', (worker, code, signal) => {
//     console.log(`工作进程 ${worker.process.pid} 已退出`);
//   });
// } else {
//   // 工作进程可以共享任何 TCP 连接。
//   // 在本例子中，共享的是一个 HTTP 服务器。
//   http.createServer((req, res) => {
//     res.writeHead(200);
//     res.end('你好世界\n');
//   }).listen(8000);

//   console.log(`工作进程 ${process.pid} 已启动`);
// }

var cluster = require('cluster');
var numCPUs = require('os').cpus().length;
 
if (cluster.isMaster) {
  console.log('[master] ' + "start master...");
 
  for (var i = 0; i < numCPUs; i++) {
     cluster.fork();
  }
 
  cluster.on('listening', function (worker, address) {
    console.log('[master] ' + 'listening: worker' + worker.id + ',pid:' + worker.process.pid + ', Address:' + address.address + ":" + address.port);
  });
 
} else if (cluster.isWorker) {
  require('./test.js');
}