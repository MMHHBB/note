# ES6笔记

## 注意点
- 用 Object.is() 判断 -0 和 0 ，NaN 和 NaN
- super() 方法其实将当前继承的子类的 prototype 指针指向父类的 prototype 对象。
- Map 类型任何类型都可以为 key, weakMap 是只接受对象为 Key ,若对于此对象只有此 weakMap 引用，此对象就会被垃圾回收。
- await要等后面的方法完全执行完，哪怕是定时器也要执行完 才执行后面的语句。代码如下
- Promise.all 其中一个挂了，会直接挂掉其他的，而 Promise.allSettled 也是并发执行，其中一个挂了不会影响其他的，返回的结果是多个结果组合成的数组

```javascript
async function a(){
    console.log(1)
    await (function(){
        return new Promise((rs)=>{
            setTimeout(()=>{
                console.log(2)
                rs()
            },3000)
        })
    })()
    console.log(3)
}

a(); // 1 2 3 
```


**requestAnimationFrame 和 requestIdleCallback**
- 浏览器一帧分为6个步骤，分别是
    1. 处理用户的交互
    2. JS 解析执行
    3. 帧开始。窗口尺寸变更，页面滚去等的处理
    4. requestAnimationFrame(rAF)
    5. 布局
    6. 绘制
![avatar](https://upload-images.jianshu.io/upload_images/3963958-432f5165ba423f57.png?imageMogr2/auto-orient/strip|imageView2/2/w/
1200/format/webp)

- 如果上述几个步骤没有用完 16 ms ，剩余时间将会执行 requestIdleCallback 。
- 用 setTimeout 执行动画的缺点，setTimeout 每次执行可能会被其他任务延迟，导致在一帧的渲染中没有进行更新，造成丢帧，影响体验。

**Proxy**
```javascript
const a = new Proxy(target,{
    get(raw,key,receiver){
        // 其中，raw 就是 target , key 就是访问的key 如下就为 xxx, receiver 就是这个参数本身，{get(){...}}
    }
})
a.xxx
```

**新增内容**
1. let const 
2. promise
3. 默认参数
4. 剩余参数运算符
5. 箭头函数
6. \`  \` 符号
7. for of
8. class 类，super extends 关键字

**箭头函数**
- 没有arguments
- 不能被 new
- 没有 prototype 

**默认参数、arguments**
- 传入 null 时不会触发默认参数。
- 默认参数也有声明顺序，如果在前一个使用了后一个的默认参数，会报错，例如：
```javascript
function foo(a=b,b){
    // dosometing...
}
foo(undefined,1);
```
- arguments 对象不包括默认参数，保存传入的参数的原始数值，若对参数进行改变，arguments 不变。

**Array.find , Array.filter 和 Array.map 有什么区别**

* 都是接受一个回调函数，参数为数组中的项，都不会改变原数组，前两个是接受 boolean 返回值。
* 区别： find 遇到第一个返回值为 true 的就结束，不会继续查找，并且将符合条件的`项`进行返回
* filter 是将查找到返回值为 true 的`项`保存到一个数组中，会继续查找，最后返回一个数组。
* map 是返回一个长度与原数组一样的数组，且`值是回调中的返回值`，如果没有返回值，就会返回 undefined。
- Promise 中在 then 中 return new Error('xxx') 对象不会触发 catch，应该 return Promise.reject(new Errpr('xxx')) 对象才可以
- CommonJS 导出的是模块的值，而 ES6 导出的是模块的引用。
- let 和 const 定义的变量不能被 window 拿到。


**怎么实现根据对象数组中的一些属性进行数组排序**

* 使用数组的 sort 方法，修改传入的回调，其中的回调的参数其实就是数组中的项，例如：
```
    let arr = [{id:1},{id:3},{id:2}];
    arr.sort( ( a,b )=>{
        return a.id - b.id;
    })
```
* sort 方法的回调，返回正值表示两个数位置调换，返回 0 或者 负值两个数位置不变。

**结构运算符是同步运算，不会使用计算后的量**

```javascript
    let [a,b]=[5,10]
    [a,b]=[b,a+b];
    console.log(a) // 10
    console.log(b) // 15,此处不会是25，因为里面的计算是同时计算，使用的 a 是没改变之前的 a
```

**class**

* class 里面不能直接声明变量，不能使用 this 会报错，但是可以直接一个变量。
```javascript
class A{
    this.xxx = 1;//报错
    let xxx = 1 ;//报错
    
    static st = 1; // 也可以用来声明静态变量
    xxx = 1; //正确，等价于下面写法，这是 ES7 的写法
    constructor(){
        this.xxx = 1;
    }
    
}
```
* class 不会变量提升，没有定义 class 时就使用会报错。
* 如果没添加 constructor 会默认添加 constructor 。
* 静态方法中调用的 this 指向的是 class 自身，只能使用类自身的也是 static 的值。

**class 静态变量的实现方法**

* 静态变量被 new 出来的对象引用不到。
* 静态变量或者静态方法其实就是声明在 funciton 上面的，而 new 出来的对象原型是指到 function 的 prototype 上的，所以引用不到。
 
**继承 extends**

```javacript
class A{
    a = 1;
    fn(){ console.log(this.a); }
}
class B extends A{
    a = 2;
    fn(){ console.log(this.a);}
}
var a = new B();
a.fn() // 2

以上没有问题，但是如果写了 constructor 的话，就需要 super ，例如

class B extends A{
    b = 2;
    constructor(){
        this.a = 1;  // new 时报错
        a = 1; // new 时报错
        console.log(b) // 2 ，直接在 class 中使用变量=值，和 constructor 可以一起用，这里会输出 2
        //什么都不写也会报错，继承只要用到了 constructor 都必须写 super 
    }
}
```

**super**

```javascript
class A {
  a = 'a of A';
  fn() { console.log(this.a); }
  static fn(){  console.log('static') ;}
}

class B extends A {
  a = 'a of B';
  constructor() {
    this.b = 'b'; //报错，在使用 this 之前必须要 super(); 
    super();
    console.log(super) //这样子会报错,Parsing error，引擎不知道这是什么调用
    console.log(super.a); // undefined 永远不能调用到父类的属性，静态属性也不行，但是可以调用到父类的方法
    super.fn(); //a of B  调用的时候，this 永远都指向子类，也就是 B ，就算子类也有一样的方法，这里还是调用的父类的方法
  }
  static getFatherStatic(){
      super.fn();  // static ，只有在 static 方法中的 super 才能调用父元素的 static 方法。
  }
  fn(){ console.log('fn of B'); }
}

```