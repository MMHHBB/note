|作用|命令|备注|
|-|-------:|:------:|
|创建git仓库 |git init |\$1|
|版本回退|git reset --hard HEAD^|// ^代表回退多少个版本，一个代表回退一个版本，多个代表回退多个|
|回退多少个版本|git reset --hard HEAD~N |// N代表回退多少个版本号|
|回退到指定的版本|git reset --hard code|//code 是回退的版本号|
|查看版本号|git reflog|//可以显示出版本号|
|对比当前文件跟仓库中文件的区别| git diff HEAD -- filename |// filename 代表文件名称 需要包括后缀名|
|修改回退|git checkout -- filename| // 没add前会回退到跟仓库一样的状态，add后会回退到add的状态|
|工作区删除，仓库也确定删除|git rm filename |//删除后还需要commit|
|关联远程仓库|git remote add origin  xxx|// xxx为远程仓库地址|
|切换远程仓库|git remote set-url origin  xxx||
|删除远程仓库|  git remote rm origin|// 切换仓库可以先删除后重新关联 |
|查看远程仓库分支和标签|git ls-remote||
|第一次push时命令 |              git push -u origin master   ||
|查看分支|                       git branch||
|本地分支同步远程分支|            git branch --set-upstream-to=origin/test2 test2|
|创建并切换分支|                  git checkout -b 分支名||
|切换分支 |                      git checkout 分支名 |     //使用此命令可以快速创建和切换分支|
|删除分支|                       git branch -d 分支名||
|删除远程分支 |                  git push origin :分支名||
|同步本地分支和远程分支|          git remote prune origin||
|合并其他分支到当前分支    |      git merge 分支名    | //合并之后不用commit 直接push，合并冲突问题： 需要手动解决，修改冲突的文件，然后直接add冲突的文件，commit 之后就可以解决冲突|
|获取远程仓库内容 |               git pull origin master|  //需要先关联，关联之后可以直接pull|
|存储当前的代码状态 |             git stash||
|查看存储的代码状态|              git stash list||
|回复存储的代码状态 |             git stash apply  / git stash pop |   //第一种是恢复了不删除stash的内容，第二种删除|
|指定需要恢复的代码 |             git stash apply stash@{x}  | //使用 stash list 查看的 stash 编码|
|分支打标签 |                    git tag xxx|    //xxx标签名|
|查看标签|                       git tag||
|添加标签|                       git tag xxx|    //xxx标签名|
|推送标签到远程仓库 |             git push --tags||
|删除标签 |                      git tag -d xxx  ||
|删除远程仓库标签|                git push origin :refs/tags/<tagname> ||
|查看commit id|                  git log --pretty=oneline --abbrev-commit||
|给某一次commit打标签  |          git tag v0.9 xxx     | //xxx为commit id|
|标签添加说明信息 |               git tag -a tagname -m "xxx" 1094adb     |//tagname 为标签名   xxx为说明信息|
|清理本地已删除分支|git remote prune origin||
