## 注意点

- https://juejin.im/post/5d9d386fe51d45784d3f8637#heading-25
- \$el 就是根据 vue 根据虚拟 dom 构建的 html dom。
- v-for 中的 key 会影响性能
- vue 创建一个可响应对象 const store = Vue.observable({ userInfo: {}, roleIds: [] })
- v-for 比 v-if 优先级高,如果要同时使用，最好使用 computed 返回一层过滤后的数据再放到 v-for 里面
- nextTick 是什么？ nextTick是在下一次DOM更新接触之后再执行的回调。
- vuex 刷新数据会丢失，使用 vuex-persist 插件可以持久化数据。

**样式穿透的三种方法**
- less使用  `/deep/`  scss使用 `::v-deep`  stylus使用 `>>>`

**动态设置 watche 监听**
```javascript
const watcher = this.$watch('xxx',()=>{},{deep:true} ) // 参数分别是 监听属性名、 回调、监听配置
watcher();// 调用返回值关闭 watcher ，取消监听
```

**小技巧**
```javascript
const router = new VueRouter({
    routes: [{
        path: '/user/:id',
        component: User,
        props: true  // 添加这个属性，也可以传入回调👇
        /*  props:(route)=>({ id: route.query.id })  */
    }]
})
<template>
    <div>
        <child :aaa.sync="aaa" @hook:updated="onChildUpdate" aa="123"></child> 
        // sync 的 作用是让组件可以使用 $emit('update:xxx') 来更新值，跟 v-model 类似
        // @hook:* 可以监听子组件的生命周期函数
        // aa 属性会渲染在生成的 Dom 上面，并且会覆盖子组件中的 aa 属性，子组件可以在 this.$attr 上面拿到
    </div>
</template>
export default {
    props: ['id'],   // 在子组件中定义一下
    methods: {
        getParamsId() {
            return this.id   // 可以直接拿到
        },
        sayName(){ 
            console.log('name changed') 
            this.$once('hook:beforeDestroy',()=>{}); // once 代表只调用一次，hook:* 代表监听某个生命周期函数
        },
        onChildUpdate(){ console.log('childUpdated') }
    },
    watch: {
        name: {
            handler: 'sayName',
            immediate: true  // 此属性可以在组件创建的时候就执行
        }
    },
    data(){
        return {
            aaa:false
        }
    },
    components:{
        'child':{
            props:['aaa'],
            template:`<div @click="click" aa='456' >我是child 点击会改变aaa</div>`, // aa 会被父组件覆盖
            inheritAttrs:false, // 添加了这个属性就不会 继承父组件给的属性， aa 就会是 456 
            methods:{
                click(){
                    this.$emit('update:aaa',!this.aaa)  //这里可以更改调用组件时传带 .sync 的 props
                }
            }
        }
    }
}
```

**为什么Vue Data 是一个function 而不是一个对象**
- 因为一个组件可能会多次创建，会有多个实例，如果只是返回一个对象，很多组件会访问同一个对象，但是使用 function 生成的则不会。

**Vue2.x 版本由于 definedProperty 的限制，并不能实现**

1.  监听数组的长度改变 //var a = [1,2]; a.length = 3 ; 不能监听;
2.  根据数组下标改变数据项 // var a = [1,2]; a[0] = 10; 不能监听;
3.  对象直接使用 . 增加属性 //var a = {} ; a.b = 0 ; 不能监听;

- 在 2.x 版本中要实现的话，解决办法如下

1. 数组改变长度的操作不常见，所以不记录
2. this.$set(arr/obj,index/key,value); 数组最好使用 push 或者 splice 进行操作。
3. 或者在定义对象的时候继续嵌套定义需要到的 key

**自带的监听性能**

- Vue.config.performance = true

**生命周期**

- beforeCreate : Vue 实例刚创建，没有 this,data,\$el,watcher,methods,数据观测,什么都没有.
- created: vue 实例创建完成，有了 data，method，可以操作 data，有数据观测，数据变化会触发 watcher，但是不能操作 dom，虚拟 dom 还没有，但是从此时开始创建。
- beforeMount : 虚拟 dom 创建完成，开始根据虚拟 dom 构建 html dom ，虚拟 dom 构建 html dom 的 render 函数首次被调用，$el 还是 undefined。
- mounted : 构建 html dom 完成，$el 指向生成的 dom ，$el 替换 el 中标记的 dom ，完成挂载，现在可以操作 dom，可以操作 $ref。
- beforeUpdate : 在更新前进行调用，第一次 mounted 的时候不会进行调用。
- updated : 更新后进行调用，第一次 mounted 的时候不会进行调用。
- beforeDestroy : 摧毁前进行调用
- destroyed : 摧毁后进行调用。
