# WebPack 笔记

##### webpack 和 grunt 、 gulp 的区别
后者只是确定了规则，将什么什么文件转换成什么文件，而 webpack 不仅将对应文件进行转换，还可以进行统一的打包，通过一个入口文件，递归组件
进行加载和打包，可以将所有的模块打包成一个或多个 js 文件，扩展性更高，webpack 可以代替后两者的作用

1. webpack3和webpack4的区别
    1. mode/–mode参数 新增了mode/--mode参数来表示是开发还是生产（development/production）production 侧重于打包后的文件大小，development侧重于构建速度
    2. 移除loaders，必须使用rules（在3版本的时候loaders和rules 是共存的但是到4的时候只允许使用rules）
    3. 移除了CommonsChunkPlugin (提取公共代码)，用optimization.splitChunks和optimization.runtimeChunk来代替
    4. 支持es6的方式导入JSON文件，并且可以过滤无用的代码 
         ```
            let jsonData = require('./data.json');
            import jsonData from './data.json';
            import { first } from './data.json'; // 打包时只会把first相关的打进去
        ```
    5. 升级happypack插件（happypack可以进行多线程加速打包）
    6. ExtractTextWebpackPlugin调整，建议选用新的CSS文件提取kiii插件mini-css-extract-plugin，production模式，增加  minimizer 
    7. 要在命令行中使用webpack命令，需要安装 webpack-cli 才可以

2. loader 和 plugin 不同
    1. loader是使webpack拥有加载和解析非js文件的能力
    2. plugin 可以扩展webpack的功能，使得webpack更加灵活。可以在构建的过程中通过webpack的api改变输出的结果

3. webpack构建流程
    1. 初始化参数，从配置文件和shell语句中读到的参数合并，得到最后的参数,option 对象
    2. 开始编译：用合并得到的参数初始化complier对象，加载所有配置的插件(插件有个 apply 函数，挂载到complier的生命周期上),执行run方法开始编译
    3. 确定入口，通过entry找到入口文件
    4. 编译模块，从入口文件出发，调用所有配置的loader对模块进行解析翻译，在找到该模块依赖的模块进行处理
    5. 完成模块编译，得到每个模块被翻译之后的最终的内容和依赖关系
    6. 输出资源，根据入口和模块之间的依赖关系，组装成一个个包含多个模块的chunk，在把每个chunk转换成一个单独的文件加载到输出列表
    7. 输出完成，确定输出的路径和文件名，把内容写到文件系统中在以上过程中，webpack会在特定的时间点广播出特定的事件，插件在监听感兴趣的事件后
会执行特定的逻辑，改变webpack的运行结果

4. webpack 热加载执行原理
？？？？

5. 如何利用webpack来优化前端性能
    1. 压缩代码。terser-webpack-plugin 压缩js代码， mini-css-extract-plugin 压缩css代码
    2. 利用CDN加速，将引用的静态资源修改为CDN上对应的路径，可以利用webpack对于output参数和loader的publicpath参数来修改资源路径
    3. 删除死代码（tree shaking），css需要使用Purify-CSS
    4. 提取公共代码。webpack4移除了CommonsChunkPlugin (提取公共代码)，用optimization.splitChunks和optimization.runtimeChunk来代替

6. 什么是bundle,什么是chunk，什么是module?
    1. bundle > chunk > module
    2. bundle: bundle是webpack打包之后的各个文件，一般就是和chunk是一对一的关系，bundle就是对chunk进行编译压缩打包等处理之后的产出。
    3. chunk：webpack在进行模块的依赖分析的时候，代码分割出来的代码块,多个 module 打包成的代码块，可能跟 module 一对多，也可能一对一， chunk是webpack根据功能拆分出来的，包含三种情况：
        - 你的项目入口（entry）
        - 通过import()动态引入的代码
        - 通过splitChunks拆分出来的代码
    4. module:开发中的单个模块

7. webpack-dev-server和http服务器如nginx有什么区别?
    1. webpack-dev-server使用内存来存储webpack开发环境下的打包文件，并且可以使用模块热更新，他比传统的http服务对开发更加简单高效。

8. DefinePlugin
    * DefinePlugin ：允许创建一个在编译时可以配置的全局变量，可以根据环境变量设置不同的配置文件等。

    **注意**
    * 如果value是一个字符串，它将会被当做code片段
    * 如果value不是字符串，它将会被stringify(包括函数)
    * 如果value是一个对象，则所有key的定义方式相同。
    * 如果key有typeof前缀，它只是对typeof 调用定义的。

9. DllPlugin
    1. 提高打包速度，将不常改变的模块存放到 dll 文件中，需要时引用，不需要打包。
    2. 使用DllPlugin可以减少基础模块编译次数，动态链接库插件，其原理是把网页依赖的基础模块抽离出来打包到dll文件中，当需要导入的模块存在于某个dll中时，
这个模块不再被打包，而是去dll中获取。在dll中大多包含的时常用的第三方模块，只要这些模块版本不升级，就只需要被编译一次。

   **注意**
   * DllPlugin参数中的name必须要和output.library值保持一致，并且生成的mainfest文件中会引用output.library值

10. happyPack开启多线程loader转换
    1. 也是加快打包速度，开启多线程进行打包。
    2. 运行在node.js之上的webpack时单线程模型，也就是只能一个一个文件进行处理，不能并行处理，happypack可以将任务分解给多个子进程，
    3. 最后将结果发给主进程，js是单线程模型，只能通过这种多线程的方式提高性能

**compiler 和 compilation 之间的区别:**

* compiler 对象代表的是不变的webpack环境，是针对webpack的
* compilation 对象针对的是随时可变的项目文件，只要文件有改动，compilation就会被重新创建。
    

**webpack 编译过程**

1. 初始化配置参数：读取文件中的配置参数，跟命令执行的参数合并，然后初始化插件。
2. 实例化 compiler : 用上一步的配置实例化 Compiler 负责文件监听和启动编译、包含了完整的 Webpack 配置，全局只有一个 Compiler 实例。
3. 加载插件 ： 调用插件的 apply 方法，让插件监听事件节点，给插件传入 compiler 实例的引用，使插件通过 compiler 调用 Webpack 提供的 API。
4. 开始编译 : Compiler.run 方法
5. 生成 Compilation : 根据 Compiler 生成 Compilation ，开发模式下会监听文件，文件改变则重新生成 Compilation，
6. compilation 阶段调用了 Loader 完成了每个模块的转换操作
7. make : Compilation 创建完毕，即将从 Entry 开始读取文件，根据文件类型和配置的 Loader 对文件进行编译，编译完后再找出该文件依赖的文件，递归的编译和解析，生成 chunk
8. 输出阶段： 将生成的 chunk 打包合成一个 js 文件

疑问：到底是 compilation 阶段就开始进行编译还是从 make 阶段开始，如果从 compilation 开始为什么 make 阶段又开始一次，如果从 make 开始，
为什么 compilation 可以知道要加载什么模块而加载相应的 loader 


**loader 和 plugin 的区别**

loader只是对模块的代码进行转换,比较局限,而插件目的在于解决 loader 无法实现的其他事，plugin可以在任何阶段调用,能够跨Loader进一步加工Loader的输出,执行预先注册的回调,使用compilation对象做一些更底层的事情。


**一些注意点**

* style-loader 作用是将转换后的 css 转换成 style 标签加入到 HTML 文件中
* use 可以使用数组，顺序是从右到左依次转换 
* 在项目目录下需要使用 npm init   ,   npm install --save-dev webpack 
* entry 中的键名就是 output 中的 filename 键中的值的 [name] 例：
* entry : { xxx:'./src/index.js'}  output : { filename : '[name].bundle.js '}  其中的 name 就为 xxx ,生成的文件为 xxx.bundle.js
* 需要将全局变量打包成模块，需要使用 externals 属性将全局打包成模块进行引入 例：
* module.exports = { output:{...}, externals:{ xxx: "jQuery" }}   xxx为模块名，可以任意取，在自己的 js 中可以直接 require("xxx") 导入
* entry 为对象时，有多少个键就会生成多少个打包后文件，每一个打包后的文件都会走 output 这个对象里的配置
* webpack 中的 clean-webpack-plugin 插件导入时首字母需要大写切用花括号包围，要不然会报错


**rules 数组中，一个文件可以多个 loader ，使用数组 例如**

打包 css 时，有多少个 css 文件就会打包出多少个 style 标签，可以更改，就如下面的例子

    module :{
       rules: [{ test:/\.css$/ , use: [
        { loader : "style-loader",
          options : { singleton: true }   // 此行将多个 css 处理为单个 style 标签,如果不加此配置，有多少个css 文件就会打包出多少个 style 标签
        },
        { loader : "css-loader" } 
      ] 
    }]}


**常用插件和 loader**

* js深度抖动插件 webpack-deep-scope-plugin  打包时可以去除没有用到的代码 ,terser-webpack-plugin 压缩 js 文件
* css 抖动插件   purgecss-webpack-plugin
* css 单独打包插件  mini-css-extract-plugin  会在 dist 中生成单独的 css 文件
* less 和 scss 文件使用 style-loader!css-loader!sass-loader?sourceMap 进行转换//从右到左 
* js 使用 babel-loader
* jsx 使用 jsx-loader?harmony
* json 使用 json-loader
* 图片文件使用 url-loader 匹配规则  /\.(png|jpg|gif|svg)$/ 
* svg , ttf , eot 等文件使用 file-loader
* 根据模板生成 html 文件 插件 htmlPlgin ，为什么要使用这个插件，因为一般打包好的 js 文件都应该使用 hash 值命名，如果使用固定的 html ,
文件来引入打包后的 js 是不容易引入的，所以需要生成 html 文档，可以动态引入打包好的 js 文件，除此之外还可以对生成的 html 文件进行压缩
* 清理打包文件夹的插件 CleanWebpackPlugin  //每次打包都会清理之前打包的文件
- svg-sprite-loader 将加载的 svg 图片拼接成 雪碧图，放到页面中，其它地方通过 <use> 复用
-  (测试无效,不知为何) new webpack.optimize.ModuleConcatenationPlugin(); 插件,可以压缩打包后的代码体积. 


# 样例

    const path = require("path");
    const htmlPlgin = require("html-webpack-plugin");
    const { CleanWebpackPlugin } = require("clean-webpack-plugin");
    module.exports = {
      entry: "./src/index.js",
      output: {
        path: path.resolve(__dirname, "dist"),
        filename: "js/[name][hash:5].js",
        chunkFilename: "[id].js"
        // publicPath:'xxx.com'  这是 cdn 地址
      },
      devtool: "cheap-module-eval-source-map", // 开发环境配置，更快定位错误
      devtool: "cheap-module-source-map", // 线上生成配置，更快定位错误
      module: {
        rules: [
          {
            test: /\.(png|jpg|gif|jpeg)$/,
            use: {
              loader: "url-loader",
              options: {
                outputPath: "imgs/",
                limit: 10000,
                name: "[name].[hash:7].[ext]"
              }
            }
          },
          {
            test: /\.css$/,
            use: [
              {
                loader: "style-loader",
                options: {
                  singleton: true //将所有 css 打包到一个 style 标签中
                }
              },
              "css-loader"
            ]
          },
          {
            test: /.html$/,
            use: [
              {
                loader: "html-loader",
                options: {
                  attrs: ["img:src"] // 解析 html 中的 img 标签
                }
              }
            ]
          },
          {
            test: /\.(eot|woff2?|ttf|svg)$/, //字体文件
            use: [
              {
                loader: "url-loader",
                options: {
                  name: "[name]-[hash:5].min.[ext]",
                  limit: 5000, // fonts file size <= 5KB, use 'base64'; else, output svg file
                  publicPath: "fonts/",
                  outputPath: "fonts/"
                }
              }
            ]
          },
          {
            test: /\.scss$/, //解析 scss 文件
            use: [
              "style-loader", // 创建style标签，并将css添加进去
              "css-loader", // 编译css
              "postcss-loader",
              "sass-loader" // 编译scss
            ]
          }
        ]
      },
      externals: {
        xxx: "jQuery"
      },
      plugins: [
        new htmlPlgin({
          filename: "index.html", //生成的html 文件名称
          template: "page.html", //HTML 模板
          hash: true, //防止缓存
          minify: {
            removeComments: true,
            collapseWhitespace: true, //去除空格和回车
            removeAttributeQuotes: true //去除引号
          }
        }),
        new CleanWebpackPlugin()
      ],
      devServer: {
        contentBase: path.resolve(__dirname, "dist"),
        port: 9090,
        host: "localhost",
        overlay: true,
        compress: true
      },
      resolve: {
        extensions: [".js", ".jsx", ".css"], // 此选项可以在 js 文件中导入模块时不用加扩展名，会以此匹配此数组的扩展名
        alias: {
          //为模块添加别名，使在打包时更快找到模块
          "@": path.join(__dirname, "src/"), //其中 src 可以为 src 也可以为 src/ 但是在 js 中导入都必须加 @/xxx 进行导入
          xx: path.join(__dirname, "src/test")
        }
      }
    };
