# Css 笔记

## 注意点

-   [小技巧地址](https://juejin.im/post/5da3a357f265da5b6723ee1e)
-   float 之后就需要设置宽度，可能变成了 inline-block。
-   子元素的 padding 四个方向全都是相对于父元素的宽度决定的，设置为 百分比是根据父元素宽度，父 width:100px 子 padding: 50% 50%; // 50px 50px
-   父元素相对定位，那绝对定位下的子元素宽高若设为百分比，是相对于父元素的(content + padding)值, 注意不含 border
-   修改 input 中光标的颜色 caret-color: red;
-   移除被点链接的边框 ：a {outline: none} a {outline: 0}
-   display none 和 visibility hidden 的区别，前者不会被构建在 render 树中，后者会，而且后者会继承，子元素如果设置为 visible 也会进行显示。
-   absolute 同时设置 left 和 right 会隐式设置宽度。
-   垂直居中除了常用的那些还有 父元素 flex 子元素 margin:auto;
-   div 中的文字自动换行 word-break:break-all;
-   （>） 选择器大于 class 选择器
-   div 中文字换行 word-wrap:break-word; 强制换行：word-break: break-all; 两者区别：word-wrap:break-word 会首先起一个新行来放置长单词，新的行还是放不下这个长单词则会对长单词进行强制断句；而 word-break:break-all 则不会把长单词放在一个新行里，当这一行放不下的时候就直接强制断句了。还有一个 white-space 强制不换行。
-   使用 flex 居中时，子元素过大不会出现滚动条，会被遮住，子元素需要设置 margin: auto;
-   行内元素设置 overflow:hidden 会偏移位置，是因为设置了之后 vertical-align 与其他的不一样，所以需要将 vertical-align 调整与其他一样。
-   优先级：!important > 行内 > id > class = 属性 = 伪类(:hover) > 标签 = 伪元素(::after) > 通配符（\*）
-   常见能自动继承的属性： text-align ,text-indent,line-height, (vertical-align 不能继承)
-   float 是不完全脱离文档流，因为它还在父元素中，且文字也会环绕它。
-   border 可以设置颜色（默认颜色与 color 颜色相同），margin 可以设置为 负值， padding 有背景颜色。
-   FOUC (Flash of Unstyled Content) ：无样式文档闪烁，现象是页面闪烁，导致原因是当样式表晚于结构性html加载时（如 style 在 body后面，），加载到此样式表时，页面将会停止之前的渲染。等待此样式表被下载和解析后，再重新渲染页面，期间导致短暂的花屏现象。
-   a 标签选择器 :visited 指示这个“超链接”已被访问，:link 表示这个“超链接”未被访问
-   两个inline-block元素放到一起会产生一段空白的原因： 因为元素被当成 inline-block 放置的时候，元素之间的空白符（空格，回车换行等）都会被浏览器处理
-   超链接访问过后hover样式就不出现，因为被点击访问过的超链接样式不再具有hover和active了 ，解决方法是改变css属性的排列属性：L-V-H-A    a:link{} → a:visited{} → a:hover{} → a:active{}

**Sass 怎么写循环**

```javascript
// @for 是 Sass 自带的命令
@for $i from 2 through 8 {
.com-hwicon {
 > div:nth-child(#{$i}) {
   position: relative;
   float: right;
  }
 }
}
```

**垂直居中**

```javascript
// 第一种
父元素 position: relative;
子元素
position:absolute ;
left:50%; top:50%;
transform:translate(-50%,-50%);
或者 子元素
position : absolute;
margin : auto;

//第二种
父元素 display:flex ;
align-item:center ;
justify-content: center;

//第三种
父元素 display:table-cell;
vertical-align:middle;
```

**rem 和 em 的区别**

-   rem 是相对于 html 根元素的字体大小(16px)来换算成 px 的单位，例如： html{ font-size : 15px; } div{ width: 2rem ;} // 此时 div 的宽度为 30px
-   em 是相对于当前元素的字体大小设定的，例如 ： div { font-size: 10px; width: 1em ; } // 此时 div 的宽度为 10px

**为什么 css 选择器从右到左进行解析**

-   因为从右到左进行解析会减少选择器匹配次数，不容易遗漏共有属性，优化性能等

**nth-child(odd/even)**

-   odd 是奇数行， even 是偶数行

**左右固定中间自适应布局**

1. 父元素 display:flex 左右固定宽高，中间 flex:1 。
2. 左右 float ，中间 margin 左右，缺点：中间节点要放在后面，渲染不好。
3. 左右 absolute 中间 margin 。
4. 圣杯布局（中间元素在前面，优先渲染）：左中右都浮动,父元素 padding 左右宽度，左 margin-left:-100%,left:-宽度 , 右 margin-right:-100%，因为看左右其实没有换到下一行，还是在中间的最后面的位置，所以这样子调整是有用的,为什么右边不直接 相对定位出去，因为右边在流中的位置还是在下面，并没有在上面，相对定位只是调整其视觉位置，实际的位置还是在下面。
5. 双飞翼布局（中间元素在前面，优先渲染）：左中右都浮动，左 margin-left:-100% 右 margin-left:-宽度回到原来位置，中间节点再包一个节点放内容，设置 margin 左右宽度就好了。

**清除浮动的方法**

1. 兄弟节点设置 clear:both;只能清除相邻的前一个浮动层级，必须要相邻如果隔了一个块层级则浮动层级清除不到。
2. 一样也是 clear:both; 但是设置在 ::after/:after 伪元素来清除浮动。conten:"",display:block;visibility:hidden;height:0;
3. 其他节点 overflow:auto/hidden; 严格来说这并不是清除浮动，而是让设置了此属性的节点不受浮动的影响，设置了此属性后，不会他`前面浮动的兄弟节点`遮住，自身属性不改变。

**靠右小技巧**

-   行内元素使用 text-align-last:justify;(存疑)
-   块：父元素 flex ,需要靠右的元素 margin-left:auto;

**等高布局**

1. flex 需要等高的子元素 flex:1
2. 父元素 display:table; 子元素设置高度的 div 设置 display:table-cell;
3. 父元素 overflow:hidden ，父元素不设置高度，子元素全部 float ，padding-bottom:9999px,margin-bottom:-9999px //两个值必须相同
    - 上面这一条的原理就是，子其实很高，但是由于父 overflow:hidden 和子 margin-bottom:-9999px 一起作用将多余部分给遮住了，而只要一个子元素增大，将父元素撑大，其他子元素就会露出 padding ，padding 是有背景颜色的，所以看起来就像是等高，但是实际上其他的子 content 还是不变的。缺点：因为 padding 很大，将 boder 挤到下面去了，所以看不到 border ,但是子 border 的高度会影响到父的高度，父的高度也会随着子高度增加而增加。

**background-attachment**

-   背景图片附着，local 属性是背景图片随着附着在设置了此属性的 div 上，fixed 是附着在 html ，相当于设置了 position:fixed 但是只是背景图片。

**BFC(Block Format Context)**

-   使自己变成 BFC 的方法：overflow:不为 visiable ,display:table-cell || table-caption || inline-block ，float :不为 none ，position:absolute ||fixed。
-   当一个元素不是 BFC 时：子元素 float 时父自身会塌陷，自身的高度根据子元素的 border 的外延来计算，所以高度不会包括子元素的上下 margin （上下边界 margin，如果不是边界当然还是包括的） 。
-   当一个元素是 BFC 时：子元素 float 父元素不会塌陷，自身的高度是根据子元素的 margin 的外延计算，所以高度会包括上下 margin ，如果 margin 设置为负值，父元素高度也会缩减。
-   BFC 就是页面上的一个隔离的独立容器，容器里面的子元素不会影响到外面的元素。反之也如此。
-   计算 BFC 的高度时，浮动元素也参与计算。

**兄弟元素 margin 上下重叠**

-   造成原因是两个 div 在同一个 BFC 中，消除此效果只需要将两个 div 不在同一个 BFC 中就好了：将其中一个单独变成一个 BFC。
