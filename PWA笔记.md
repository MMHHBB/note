# PWA 笔记

**PWA全称Progressive Web App，即渐进式WEB应用。**

1. 可以添加至主屏幕，点击主屏幕图标可以实现启动动画以及隐藏地址栏
2. 实现离线缓存功能，即使用户手机没有网络，依然可以使用一些离线功能
3. 实现了消息推送

## Service worker 

**是什么？**

* 是一个 Web Worker ，可以独立与主线程之外，可以监听 APP 对后端的请求，可以使用本地 Storage API ，所以可以实现离线访问，后台同步和 Web Push。在页面中注册并安装成功后，运行于浏览器后台，不受页面刷新的影响，可以监听和截拦作用域范围内所有页面的 HTTP 请求。

**特点**

* 网站必须使用 HTTPS。除了使用本地开发环境调试时(如域名使用 localhost)
* 运行于浏览器后台，可以控制打开作用域范围下所有的页面请求
* 单独的作用域范围，单独的运行环境和执行线程
* 不能操作页面 DOM。但可以通过事件机制来处理
* 事件驱动型服务线程

**为什么要求网站必须是HTTPS**

* 大概是因为service worker权限太大能拦截所有页面的请求吧，如果http的网站安装service worker很容易被攻击

**缺点**

* 不能操作 DOM 
* 必须使用 HTTPS
* 各大浏览器支持率不高

##  Web App Manifest

**是什么？**

* 它是一个外链的 JSON 文件，在这个文件中，向浏览器暴露了站点的名称，地址，图标等等元数据。
* 优点：可以生成桌面图标，提高用户粘度。
* 缺点：需要浏览器拥有创建桌面图标权限，默认的浏览器是关闭的。
* 例子
    ```javascript
    <link rel="manifest" href="/manifest.json">  //html 中引入
    {
      "name": "百度天气",
      "short_name": "天气",
      "icons": [
        {
          "src": "/dist/static/img/icons/android-chrome-192x192.png",
          "sizes": "192x192",
          "type": "image/png"
        }
      ],
      "start_url": "/?from=homescreen",
      "background_color": "#3E4EB8",
      "display": "standalone",
      "theme_color": "#2F3BA2"
    }
    ```
