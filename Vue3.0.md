# Vue3.0笔记

## funciton api
- const a = ref(xxx) 的返回值 a 需要使用 a.ref 才能获取 xxx 的值，因为 vue 的底层 Object.defineProperty 只能作用于对象，不能作用于基本类型，但是在 template 中使用的时候可以不需要 a.ref 在 template 中会自动解析。
- setup() 的返回对象就是注入模版中的变量，例如 <div>{{a}}</div> 在 setup() 中就需要 return {a}
