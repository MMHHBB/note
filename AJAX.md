# AJAX 笔记

**状态码**

* 0(UNSENT):未初始化。尚未调用open()方法
* 1(OPENED):启动。已经调用open()方法，但尚未调用send()方法
* 2(HEADERS_RECEIVED):发送。己经调用send()方法，且接收到头信息
* 3(LOADING):接收。已经接收到部分响应主体信息
* 4(DONE):完成。已经接收到全部响应数据，而且已经可以在客户端使用了


```javascript
let xhr = new XMLHttpRequest;

xhr.onreadystatechange = function(){   //当状态码改变时会触发此监听，需要先指定这个方法，再进行发送
    if(xhr.readyState == 4){ // 数据接收完成，可以使用
        if(xhr.status === 200){  //状态码
            //Doing
        }
    }
}
xhr.timeout = 2000;   //超时时间，默认为0，表示没有时间限制，不设置会一直在等待接收状态
xhr.ontimeout  = function(){  //超过时间后的回调
    //Doing
}

xhr.open("get", "example.txt", false);  //方法名 ， URL  ，是否异步（默认为 false ）
xhr.send(null);   //如果是 get 方法可以不用设置或者设置为 null ，其他方法需要设置

```