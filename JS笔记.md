# JS 笔记

## 注意点

- Number(null) // 0 ；Number(undefined) // NaN ；Number(Symbol()) // 报错
- typeof NaN === 'Number' // true    typeof null === 'Object' // true
- 1 + Symbol() || '1' + Symbol() // 报错  Boolean(Symbol()) // true  String(Symbol()) // "Symbol()" 
- setTimeout 是过了多少秒之后把任务丢到任务队列，而不是立即放入任务队列。
- [1,2,NaN].indexOf(NaN) //-1 不能获取 NaN 的位置，但是 [1,NaN].includes(NaN) //true includes 是可以获取到的
- a 函数作为参数传入 b 函数中，b 函数通过 arguments[index] 来调用 a 函数，此时 this 的指向为 arguments; 如果是直接调用 a() ，此时 a 中 this 的指向不变，并不会指向所在的 b 函数的 this。
- 在对象中的方法执行一个立即执行函数，其中的 this 指向 window 。
- delete 数组中的某个元素，那个元素会变成 undefined ，数组长度不会减小 例：var a = [1,2,3]; delete a[0]; // a=== [undefined,2,3]
- 截断 a 标签被点击的跳转，在捕获阶段，preventDefault()，就不会触发 a 标签的跳转。
- window.onload 等文档和资源都加载完成以后调用 。document.ready 只要 dom 树加载完成后就会调用， document.ready 比 window.onload 快。
- (-0).toString() // 结果为"0" 。不能转换 - 号
- BOM对象有哪些：Location，Document，History，Screen，Navigator，window
- 隐式转换： +'1' // 1 这样子转换成数字速度最快。
- Object.create(obj) 返回的对象的 __proto__ 指向传入的 obj， Object.assign(obj) 返回的对象的 __proto__ 指向 obj 的 __propto__ .
- Object.keys 只能得到可枚举的属性名，Object.getOwnPropertyNames 可以得到不可枚举的属性。但是两者都不能得到原型链上的属性。
- 用 JSON.parse(JSON.strify) 复制的缺点，不能复制 function、正则表达式类型和循环引用。
- setTimeout 最小延迟是 4ms ,其实是 setTimeout 嵌套层级多才是 4ms , 然后因为如果是 1ms 会导致耗电非常快,且在一些浏览器会导致 bug ,所以设置为 4ms .

- 使用 var 添加到 window 的属性，不能通过 delete 删除。
- 'a'-'b'+'2'  // NaN2  前面运算得到的 NaN 为字符串，后面进行拼接
- 使用 visibilitychange 事件来监听浏览器标签是否显示，window.addEventListner('visibilitychange',()=>{});
- document.visibilityState === 'hidden' || 'visible' 获取当前标签的是否展示
- 浏览器中的一帧 ![Alt text](https://mk2048.oss-cn-beijing.aliyuncs.com/web_upload/blog_imgs/200/https___user-gold-cdn-xitu-io_2019_12_1_16ec137dbbba1de5_imageView2_0_w_1280_h_960_ignore-error_1.gif)


**事件冒泡**
- 事件对象中有 bubbles 属性决定此事件是否可以冒泡。




**浏览器输入 url 之后发生了什么**
- 先进行 DNS 解析，DNS 解析先查浏览器是否有 DNS 缓存，再查系统缓存，如果都没有就查 DNS 服务器，一直向上查到根 DNS 服务器，查到之后得到 IP，存入本地缓存中，然后到网络层通过 IP 建立 TCP连接，经过三次握手，建立之后连接后，通过 url 中的协议发起请求，如果是 http 协议，获取请求服务，如果获取的是字符串，则浏览器直接根据字符串进行渲染，如果是 html 文件，则解析 html 文件，开始构建 DOM 树，解析到相应的标签则根据标签中的链接发起请求，请求 Link 链接中的 Css 文件，获取到后解析，构建 CSSOM树，构建完成后合成 Render 树，开始布局，计算 DOM 的几何位置，最后进行绘制，页面展现。

**function**
- 每个函数都有 name 属性，普通函数为 anonymous ,getter 函数为 get setter 函数为 set ,bind 函数为 bound
- 都有 [\[construct]]方法 和 [\[call]] 方法，调用使用 call 方法，new 使用 construct 方法。
- 改变函数原型的方法 Object.setPrototypeOf(fn , obj) 或者 fn.prototype = obj

**怎么让事件先冒泡后捕获**
```javascript
dom.addEventListener('click',fn1,false) // 先注册冒泡事件
dom.addEventListener('click',fn2,true) // 再注册捕获事件
产生事件时会根据注册的顺序执行，所以会先执行第一个，再执行第二个。

例外情况 ：如果再 dom 中有 onclick 已经绑定了函数 fn3，也就是说有 Dom 0级事件的话，
就不可以达到效果，顺序为 fn2 => fn3 => fn1
```

**JS 对象中的属性的属性**
1. configurable : 是否能被删除
2. enumerable: 是否能被枚举.
3. writable: 是否能被修改。
4. get方法：访问此属性时会调用此方法，默认传入 this 对象
5. set方法：改变此属性值时调用此方法，第一个参数为 新的值， 默认传入 this 对象。
4. value : 值

**new 的过程**

```javascript
const a = new AA();
过程为以下

function newFn(){
  var obj = {};
  obj.constructor = arguments.callee;
  obj.__proto__ = arguments.callee.prototype;
  arguments.callee.call(obj);
  return obj;
}
```

**for 循环中使用 setTimeout 立即执行函数的理解**

```
for(var  i = 0 ; i < 5 ; i++){
    (function(i){
        setTimeout(()=>{
            console.log(i);
        },1000);
    })(i);
}
```

- 解释： 使用立即执行函数，会生成 5 个匿名函数，所以 setTimeout 中引用的 i 其实是匿名函数中的变量 i ，不会是全局，所以显示的 i 会是正常的 i;
- 注意： setTimeout 中的回调函数不能传 i 进去，会显示 undefined

**arguments.callee**

- 这个返回的是当前函数本身，常用于递归调用

```javascript
function fn(num) {
  if (num <= 1) {
    return 1;
  } else {
    return num * arguments.callee(num - 1);
  }
}
```

**声明对象的时候不能取当前对象内的值，会报错**

```javascript
var a = {
x : 3,
y : 9,
z : a.y + 1;
}
报错 Cannot read property 'y' of undefined
```

**valueOf 和 toString 的区别**

- valueOf 偏向于运算，toString 偏向于显示。
- 一个对象使用 == 运算符时使用的是对象的 valueOf 方法。

  1. 在进行对象转换时（例如:alert(a)）,将优先调用重写的 toString 方法。
  2. 在进行强转字符串类型时将优先调用 toString 方法，强转为数字时优先调用 valueOf。
  3. 在有运算操作符的情况下，valueOf 的优先级高于 toString。

```javascript
let a = {
  toString: function() {
    return "toString";
  },
  valueOf: function() {
    return "valueOf";
  }
};
console.log(a == "valueOf"); // true
console.log(a == "toString"); // false
console.log(a + 1); //valueOf1
alert(a); // toString
```

**ES6 之前怎么判断一个 function 是被 new 还是调用**

- 判断依据是根据 this 的指向，如果是调用，指向就是 window ，如果是 new 就是构造的对象。

```javascript
function fn(){
    this.__isCreated = false;
    //前面两个是判断是否是 new 的条件，后面一个是标志量，防止调用方法时也以为是 new
    if(this instanceof arguments.callee || this.constructor === arguments.callee && !this.__isCreated ){  // new
        this.__isCreated = true;   //此标志量判断是否被 new 过，免得调用其中的方法时会出错。
    }
}
缺点就是
let a = new fn();
a.xxx = fn;
a.xxx(); // new
解决这个问题就是加个标志量,如代码中的 __isCreated

new.target 的使用
function A(){
    if(!new.target) console.log("错误！此函数必须要 new 调用"); //不是 new 时 new.target 为undefined
    else console.log("此函数目前是被 new 调用") //new 时 new.target 得到的是一个函数体，可以被调用 new.target(123)，是可以的
}
```

**['1','2','3'].map(parseInt) // 1 NaN NaN 的原因**

- parseInt 是回调函数，而 map 会传三个参数给回调函数，分别是 item index this ，在此处 item 为 ‘1’ index 为 0 this 为调用 map 的那个数组，以此类推
- 而 parseInt 的参数为 ( String , 进制数（范围 2 ～ 36，或者 0 ,为 10 进制）)
  ```parseInt('1',0,this) // 1
  parseInt('2',1)  // 1 进制解析 '2'，失败会返回 NaN
  parseInt('3',2)  // 2 进制解析 '3'，失败会返回 NaN
  所以最后结果为 1 NaN NaN
  ```
- 例 2 ： ['1','3','10'].map(parseInt) // [1,NaN,2] 最后是二进制解析 '10' 所以为 2

**AST 是什么**

- AST 是 Abstract syntax Tree 抽象语法树，是 JS 的底层知识，再往下就到了往低级语言翻译了。
- 其中有两个阶段，词法分析阶段和语法分析阶段。
- 词法分析阶段：扫描代码，它遇到空格，操作符，或者特殊符号的时候，它会认为一个话已经完成了，将代码分析成一堆 tokens 数组，其中有不同的对象代表不同的词，
- 语法分析阶段：语法分析，也就是解析器。它会将词法分析出来的数组转化成树形的表达形式。同时，验证语法，语法如果有错的话，抛出语法错误。当生成树的时候，解析器会删除一些没必要的标识 tokens（比如不完整的括号），因此 AST 不是 100%与源码匹配的，100% 匹配的是叫做 CST 具体语法树。

```javascript
function add(a, b) {
  return a + b;
}
这一个会被分解为;
```

**怎么实现判断一个 dom 是否被用户看到**

- IntersectionObserver API [使用文件地址](http://www.ruanyifeng.com/blog/2016/11/intersectionobserver_api.html)

```
    var io = new IntersectionObserver(callback, option);
    // 开始观察
    io.observe(document.getElementById('example'));
    // 观察多个就多次调用
    is.observe(dom1); is.observe(dom2);
// 停止观察
    io.unobserve(element);

// 关闭观察器
    io.disconnect();

    //callback 的传的参数的数据解构
    {
      time: 3893.92, //可见性发生变化的时间，是一个高精度时间戳，单位为毫秒
      rootBounds: ClientRect {  //被观察的目标元素，是一个 DOM 节点对象
        bottom: 920,
        height: 1024,
        left: 0,
        right: 1024,
        top: 0,
        width: 920
      },
      boundingClientRect: ClientRect {  //目标元素的矩形区域的信息
         // ...
      },
      intersectionRect: ClientRect {  //目标元素与视口（或根元素）的交叉区域的信息
        // ...
      },
      intersectionRatio: 0.54,  //目标元素的可见比例，即intersectionRect占boundingClientRect
      target: element   //被观察的目标元素，是一个 DOM 节点对象
    }

```
