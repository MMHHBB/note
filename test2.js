const promise = Promise.resolve();
setTimeout(()=>{
  promise.then(() => {
    console.log("promise");
  });
  
  setTimeout(() => {
    console.log("setTimeout");
  }, 0);
  setImmediate(() => {
    console.log("setImmediate");
  }, 0);
  
},0)