# React-Action-笔记 

## react-thunk


- 这算是一个适配器，使 redex 的 dispatch 方法可以接受 function 类型的 payload，所以可以传入异步函数。
- 使用方法：
    1. 只需要将其使用在 react 中间件中就可以。const store = createStore(reducers, applyMiddleware(thunk))
    2. 在 dispatch 中传入一个函数，第一个参数为 dispatch 方法，第二个参数为 getState 方法，在此函数中可以进行异步操作，再将操作的
    结果传入第一个参数 dispatch 函数中，例子：
    ```javascript
    const mapDispatchToProps = (dispatch: any) => ({
        setMessageData: (data: any) =>
        dispatch((dispatch: Function) => {
          window.apis.getMessageData().then(res => {
            dispatch({type:'xxxx',payload:{data:res.data}})  // dispatch 中的对象一般使用 createAction 创建。 
          })
        })
    })

    ```
    
- `注意：此异步函数最好不要写在 createAction 中，因为写在其中，到时候回调时又要调用 dispatch 方法，会需要多写一个同步 action,可以写在 mapDispatchToProps 中 `
- 此操作的返回值是 this.props.setMessageData() // undefined ，可以在异步函数中捕获错误，但是在业务代码中不好捕获错误。

## redux-promise-middleware ( 这个比上面那个好 )

- 这个跟上面一样，也是一个适配器，使 redux 的 dispatch 方法可以接受 promise 类型的 payload ，promise resolve 的值就是 action 的 payload。
但是 resolve 之后会触发一个 action,新触发的 action 的 type 是此异步 action 的 type + _FULFILLED ，如果错误了则为 type + _REJECT ，刚开始执行会触发
type + _PENDING
例如：
```javascript
//reducer 里面
handleActions<any, any>(
  {
    [`${types.fetchMessageData}_FULFILLED`]: (state, action) => {
      return {
        ...state,
        ...action.payload.data
      }
    }
  },
  initialState
)

//action中
export const fetchMessageData = createAction(types.fetchMessageData, () => {
  return window.apis.getMessageData().then(res => ({
    data: {
      messageData: res.data.map((item: any) => ({
        ...item,
        key: `${item.msg_id}`
      }))
    }
  }))
})

```
- 最后，此操作的返回值是一个 this.props.fetchMessageData() //promise，容易在业务代码中捕获错误信息