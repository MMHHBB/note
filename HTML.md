# HTML 笔记

## 注意点

- render 树不会加载 head,script 等标签，因为 render 树只是负责加载可以看到的可以显示的节点,所以跟 html 节点不是一一对应的。
- 给 dom 添加 id 会在 window 对象中添加一样的属性，指向添加 id 的那个 dom
- dom.removeChild() 方法删除节点后还在内存中，彻底删除需要 let x = dom.removeChild(); x = null;
- a 标签的 href 属性为url，如果有 download 属性，则点击后会下载 url 中的资源，其中 download 属性的值为下载后的资源名字。
- input 重新设置的值与之前的值相同时不会触发 onChange 事件。
- 滚动条是来自 html 而不是 body;
- a 标签 href="tel:139xxxxxxxx" 属性, tel 是协议号,可以调用手机拨打电话.

**muatationObserver**
- 监听 dom 改变的 API 的 observer ，只要使用了改变 DOM 的 API 都会触发，vue 和 React 的每一次 render 都会触发。

**Dom事件**
- Dom 0级事件
  1. 写在标签里的 onCLick 事件。
  2. 获取 Dom 之后操作 dom.onClick = function (){} 
- Dom 2级事件：在 JS 里添加的 addEventListener 监听的事件。

**append() 和 appendChild() 的区别**

- dom.appendChild() 不可以添加字符串，且只能添加一个节点，返回值是添加的那个节点的引用，需要添加字符串节点需要 dom.appendChild(document.createTextElement('字符串'))
- dom.append() 可以添加字符串和多个节点，没有返回值。
- 相同点：添加的元素的引用如果是页面中已经存在的元素，则存在的元素会被转移到添加的节点上。

**button 怎么设置为不可用**

- 有个 disable 属性，不管设置的值为 true 还是 false 都会导致不可用，只要设置了都会不可用，设置为空串也一样。

**table 标签和 caption 标签**

- caption 标签需要用在 table 标签中，代表的是一个 table 的标题，不会被包在表格中。

**i 标签**

- i 标签是一个 inline 标签，代表的是斜体字，被 i 标签包住会表示为斜体。

**让 HTML 识别 string 里的 '\n' 并换行**

- css 属性 white-space: pre-line;
