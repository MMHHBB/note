## 注意点

-   使用 node app.js 开启服务，如何让它在后台运行。开启守护进程，在命令后加上 & 符号，表示开启守护进程来执行。node app.js &
-   Nodejs 性能优势在于，单线程，避免了线程创建和线程切换所产生的资源开销。
-   某些异步 IO 会启用多线程，会调用 libuv 的线程池，默认启动 4 个线程 ，通过 process.env.UV_THREADPOOL_SIZE = 123 修改，默认启动的线程数量。
-   底层默认常驻线程数量为 7 ，默认启动线程池线程数量为 4。
-   Node 中的 Event Loop 和浏览器中的 Event Loop 不完全一致。
-   setTimeout(fn,0) 会被改成 setTimeout(fn,1);
-   在 a 中引用 b ，b 中引用 a 不会循环引用。
-   process.platform 判断 node 运行的系统,(window 为 'win32')
-   npm link 命令,将本地模块引入 node_modules 中,可以使用 require 进行引用.

### 模块引用

-   require 模块的时候是递归向上查找，找当前，再往上一层一直向上，

```
例如/home/ry/projects/node_modules/bar.js
/home/ry/node_modules/bar.js
/home/node_modules/bar.js
/node_modules/bar.js
```

### 事件循环

-   process.nextTick 不属于 Event loop 任何一个阶段，但是优先级最高，进入事件循环之后比其他的先执行。
-   过程如下
    1. timers :setTimeout 和 setInterval 都是将事件在此注册
    2. pending callbacks
    3. idle prepare
    4. poll
    5. check ： setImmediate 的回调会注册在这里。
    6. close callbacks ：此处结束之后继续事件循环，从上面第一点开始

### 事件驱动的优势

-   事件驱动相当于维护一个线程池，有请求的时候从线程池中取出一个线程运行完成之后通知主线程，更新数据信息，并且释放线程。

### Libuv

-   Libuv 是一个跨平台的异步 IO 库，它结合了 UNIX 下的 libev 和 Windows 下的 IOCP 的特性，Node 底层的异步 IO 就是由这个实现的，是 Node 的核心。
-   例如文件读取的异步，其实都是由 libuv 实现的，而 Node 只是 Libuv 的接口获取数据。

### Node 是单线程的吗？（此段待纠正）

-   Node 其实不是单线程的，要不然异步执行在哪执行，说它是单线程是因为它只有一个 js 引擎在主线程上执行，其他异步 IO 和事件驱动相关的线程通过 libuv 来实现内部的线程池和线程调度。libv 中存在了一个 Event Loop，通过 Event Loop 来切换实现类似于多线程的效果。简单的来讲 Event Loop 就是维持一个执行栈和一个时间队列，当前执行栈中的如果发现异步 IO 以及定时器等函数，就会把这些异步会掉函数放入到事件队列中。当前执行栈执行完成后，从事件队列中，按照一定的顺序执行事件队列中的异步回调函数。
-   Node 宿主环境是多线程的，Node 和 浏览器都是，单线程指的是只有一个 JS 引擎在主线程执行。

### 进程与线程

-   掘金文章 https://juejin.im/post/5d43017be51d4561f40adcf9
-   进程是操作系统进行资源分配和调度的最小单位，线程是程序执行流的最小单位。
-   Node 实现 IPC 通信是使用 libuv，IPC（Inter-Process Communication，进程间通信）技术有很多，如命名管道，匿名管道，socket，信号量，共享内存，消息队列。
-   启动一个 Node 服务，其实有一个进程，一个线程，一个事件循环，一个 JS 引擎实例，一个 Node.js 实例，其中 JS 引擎就是 V8 引擎，这个引擎底层实现就有多线程，主线程：编译、执行代码，编译/优化线程：在主线程执行的时候，可以优化代码。分析器线程：记录分析代码运行时间，为 Crankshaft 优化代码执行提供依据。垃圾回收的几个线程。

### child_process 模块

-   child_process.spawn()：适用于返回大量数据，例如图像处理，二进制数据处理。
-   child_process.exec()：适用于小量数据，maxBuffer 默认值为 200 \* 1024 超出这个默认值将会导致程序崩溃，数据量过大可采用 spawn。
-   child_process.execFile()：类似 child_process.exec()，区别是不能通过 shell 来执行，不支持像 I/O 重定向和文件查找这样的行为
-   child_process.fork()： 衍生新的进程，进程之间是相互独立的，每个进程都有自己的 V8 实例、内存，系统资源是有限的，不建议衍生太多的子进程出来，通长根据系统 CPU 核心数设置。

### workeer_thread 模块

-   就是单个进程，多个线程，每个线程都拥有独立的事件循环，每个线程都拥有一个 JS 引擎实例，每个线程都拥有一个 Node.js 实例，（还没正式上线，特性等待检验）

### cluser 模块

-   cluster 就是启动了多个进程，相当于多个 Node 服务，之间相互隔离，不共享内存，所以不会出现锁，通过事件进行通信，并没有子线程。
-   子进程监听端口并没有真正的监听，cluster 会根据线程执行不同的方法，子进程只是将端口和地址信息发送给主线程监听，主进程接收到请求后自动负载均衡，根据子进程的情况调用相关子线程。
-   调用时就是使用 child_process.fork 了当前使用了 cluster 的这个文件，所以会执行多次，使用 cluster.isMaster 判断哪个是主进程。
-   cluster 内置负载均衡，算法是 Round-robin 算法，进程之间的通信方式为 IPC 通信。
-   缺点：进程间通信需要通过 事件通信，不能共享内存空间。

### pm2

-   是一个模块框架，封装了底层的 child_process 和 cluster 模块，可以更好更方便地管理进程。

### 模块化

-   node 中的模块都是一个对象，导出时是 module.exports 导出，导入时通过 require 导入，得到的是导出时的 exports 对象，例如

```javascript
//a.js;
var a = { a: 1 };
module.exports = a; // require 得 {a:1}
//也可以这样
exports.xxx = a; // require 得 {xxx:{a:1}}
//但是不可以这样
exports = a; // 这样子导出无效，其他文件导入时是  undefined
```

-   模块加载机制： 第一次使用时存入缓存中，再次加载时判断缓存中是否有，有则直接使用，无则重新加载。
