# TypeScript 笔记

class 可以作为变量使用，但是 interface 不可以

- interface 同名不会报错，而且类型会将两个其中的键结合起来为一个interface，声明的变量会包含两个interface 的键，有相同的键会报错，没有的话没问题。
```
例如
interface A{a:number} 
interface A{b:string}
结果为 interface A {a:number;b:string}
```

**装饰器**
```javascript
例子
function Decorator(...args){
  return function(target:Function){ 
    //装饰器传入的参数不确定，装饰类传入 ( a :Function)
    // 装饰方法传入 ()
    // do something   
  }
}
@Decorator
function aa(){}
```